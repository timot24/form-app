/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

// import pages
import Page from "./pages/Page";
import Page1 from "./pages/Page1";
import Page2 from "./pages/Page2";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path="/" render={(props) => <Page {...props} />} exact />
          <Route path="/page-1" render={(props) => <Page1 {...props} />} />
          <Route path="/page-2" render={(props) => <Page2 {...props} />} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
