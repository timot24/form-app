/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  Container, InputGroup, InputGroupText, Button, Form, Input,
} from "reactstrap";
import { useHistory } from "react-router-dom";

export default function page1() {
  const [hasLaptop, setHasLaptop] = useState(false);
  const [fullname, setFullName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [jobDesc, setJobDesc] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [mobile, setMobile] = useState("");
  const history = useHistory();

  const clearLocalStorage = () => {
    localStorage.clear();
    alert("Thank you for submit form");
  };

  React.useEffect(() => {
    setFirstName(localStorage.getItem("firstName"));
    setLastName(localStorage.getItem("lastName"));
    setFullName(firstName + lastName);
    setJobDesc(localStorage.getItem("jobDesc"));
    setGender(localStorage.getItem("gender"));
    setEmail(localStorage.getItem("email"));
    setHasLaptop(localStorage.getItem("hasLaptop"));
    setAddress(localStorage.getItem("address"));
    setMobile(localStorage.getItem("mobile"));
  }, []);

  return (
    <Container className="d-flex mt-5 flex-column justify-content-center align-items-center">
      <Form onSubmit={clearLocalStorage} style={{ width: 400 }} className="border border-5 border-dark p-4 rounded">
        <div>
          <p><b>Confirmation data of entry</b></p>
          <div className="mt-4">
            <p>
              <b>
                Fullname:
                {fullname}
              </b>
            </p>
            <p><b>Jobdesc:  {jobDesc}</b></p>
            <p><b>Gender:  {gender}</b></p>
            <p><b>Email:  {email}</b></p>
            <p><b>Have a Laptop / PC?:  {hasLaptop === "1" ? "Yes" : "No"}</b></p>
            <p><b>Mobile Number:  {mobile}</b></p>
            <p><b>Address:  {address}</b></p>
          </div>
          <div className="mt-4 d-flex justify-content-end">
            <Button onClick={() => history.goBack()} className="button rounded w-25 mr-1" color="danger">Back</Button>
            <Button type="submit" className="button rounded w-25" color="primary">Submit</Button>
          </div>
        </div>
      </Form>
    </Container>
  );
}
