/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  Container, InputGroup, InputGroupText, Button, Form, Input,
} from "reactstrap";
import { useHistory } from "react-router-dom";

export default function page1() {
  const [hasLaptop, setHasLaptop] = useState(0);

  const [address, setAddress] = useState("");
  const [mobile, setMobile] = useState("");
  const history = useHistory();

  React.useEffect(() => {
    setHasLaptop(localStorage.getItem("hasLaptop") ? localStorage.getItem("hasLaptop") : 0);
    setAddress(localStorage.getItem("address") ? localStorage.getItem("address") : "");
    setMobile(localStorage.getItem("mobile") ? localStorage.getItem("mobile") : "");
  }, []);

  const valueRadio = {
    choices:
    [
      { text: "Yes", value: "1" },
      { text: "No", value: "2" },
    ],
  };

  const submit = (e) => {
    e.preventDefault();
    localStorage.setItem("hasLaptop", hasLaptop);
    localStorage.setItem("address", address);
    localStorage.setItem("mobile", mobile);
    history.push("page-2");
  };

  return (
    <Container className="d-flex mt-5 flex-column justify-content-center align-items-center">
      <Form onSubmit={submit} style={{ width: 400 }} className="border border-5 border-dark p-4 rounded">
        <div>
          <p><b>Information B</b></p>
          <InputGroup>
            <div className="mr-3">
              <p><b>Have a Laptop / PC?</b></p>
              <div className="d-flex flex-row ml-4">
                {valueRadio.choices.map((choice, index) => {
                  if (index === 0) {
                    return (
                      <label>
                        <Input
                          type="radio"
                          name="vote"
                          value={choice.value}
                          key={index}
                          checked={hasLaptop === choice.value}
                          onChange={() => setHasLaptop(choice.value)}
                        />
                        {choice.text}
                      </label>
                    );
                  }
                  return (
                    <label className="ml-5">
                      <Input
                        type="radio"
                        name="vote"
                        value={choice.value}
                        key={index}
                        checked={hasLaptop === choice.value}
                        onChange={() => setHasLaptop(choice.value)}
                      />
                      {choice.text}
                    </label>
                  );
                })}
              </div>
            </div>
          </InputGroup>
          <InputGroup className="d-flex flex-column mt-3">
            <p><b>Address</b></p>
            <div>
              <div className="d-flexjustify-content-center align-items-center">
                <textarea className="rounded w-100 " name="address" value={address} onChange={(e) => { setAddress(e.target.value); }} required />
              </div>
            </div>
          </InputGroup>
          <InputGroup className="d-flex flex-column mt-3">
            <p><b>Mobile Number</b></p>
            <div>
              <div className="d-flex justify-content-center align-items-center">
                <Input type="number" className="rounded" name="mobile" value={mobile} onChange={(e) => { setMobile(e.target.value); }} required />
              </div>
            </div>
          </InputGroup>
          <div className="mt-4 d-flex justify-content-end">
            <Button onClick={() => history.goBack()} className="button rounded w-25 mr-1" color="danger">Back</Button>
            <Button type="submit" className="button rounded w-25" color="primary">Next</Button>
          </div>
        </div>
      </Form>
    </Container>
  );
}
