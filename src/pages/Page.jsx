/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  InputGroup, InputGroupText, Container, Alert,
  Button, Form, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from "reactstrap";
import { useHistory } from "react-router-dom";

export default function page() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [jobDesc, setJobDesc] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const history = useHistory();

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  React.useEffect(() => {
    setFirstName(localStorage.getItem("firstName"));
    setLastName(localStorage.getItem("lastName"));
    setJobDesc(localStorage.getItem("jobDesc"));
    setGender(localStorage.getItem("gender"));
    setEmail(localStorage.getItem("email"));
  }, []);

  const submit = (e) => {
    e.preventDefault();
    localStorage.setItem("firstName", firstName);
    localStorage.setItem("lastName", lastName);
    localStorage.setItem("jobDesc", jobDesc);
    localStorage.setItem("gender", gender);
    localStorage.setItem("email", email);

    history.push("page-1");
  };

  return (
    <Container className="d-flex mt-5 flex-column justify-content-center align-items-center">
      <Form onSubmit={submit} className="border border-5 border-dark p-4 rounded">
        <div>
          <p><b>Information A</b></p>
          <InputGroup>
            <div className="mr-3">
              <p><b>First Name</b></p>
              <Input type="text" className="rounded" name="firstName" value={firstName} onChange={(e) => { setFirstName(e.target.value); }} required />
            </div>
            <div className="ml-3">
              <p><b>Last Name</b></p>
              <Input type="text" className="rounded" name="lastName" value={lastName} onChange={(e) => { setLastName(e.target.value); }} required />
            </div>
          </InputGroup>
          <InputGroup className="d-flex flex-column mt-3">
            <p><b>Jobdesc</b></p>
            <div>
              <div className="d-flex justify-content-center align-items-center pr-3 pl-3">
                <Input type="text" className="rounded" name="jobDesc" value={jobDesc} onChange={(e) => { setJobDesc(e.target.value); }} required />
              </div>
            </div>
          </InputGroup>
          <InputGroup className="d-flex flex-column mt-3">
            <p><b>Gender</b></p>
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle className="p-0" style={{ width: "100%", backgroundColor: "white", borderColor: "white" }}>
                <Input className="w-100 rounded" value={gender} required disabled />
              </DropdownToggle>
              <DropdownMenu
                modifiers={{
                  setMaxHeight: {
                    enabled: true,
                    order: 890,
                    fn: (data) => ({
                      ...data,
                      styles: {
                        ...data.styles,
                        overflow: "auto",
                        maxHeight: "100px",
                        width: "405px",
                        alignItems: "center",
                        left: 0,
                      },
                    }),
                  },
                }}
              >
                <DropdownItem onClick={() => setGender("MALE")}>MALE</DropdownItem>
                <DropdownItem onClick={() => setGender("FEMALE")}>FEMALE</DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </InputGroup>
          <InputGroup className="d-flex flex-column mt-3">
            <p><b>Email</b></p>
            <div>
              <div className="d-flex justify-content-center align-items-center">
                <Input type="email" className="rounded" name="email" value={email} onChange={(e) => { setEmail(e.target.value); }} required />
              </div>
            </div>
          </InputGroup>
          <div className="mt-4 d-flex justify-content-end">
            <Button type="submit" className="button rounded w-25" color="primary">Next</Button>
          </div>
        </div>
      </Form>
    </Container>
  );
}
